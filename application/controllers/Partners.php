<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Partners extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Partners_model');
        $this->load->library('form_validation');

        if(!$this->session->userdata('logined') || $this->session->userdata('logined') != true)
        {
            redirect('/');
        }        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('partners/partners_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Partners_model->json();
    }

    public function read($id) 
    {
        $row = $this->Partners_model->get_by_id($id);
        if ($row) {
            $data = array(
		'id' => $row->id,
		'language_code' => $row->language_code,
		'fullname' => $row->fullname,
		'title' => $row->title,
		'description_1' => $row->description_1,
		'description_2' => $row->description_2,
		'photo' => $row->photo,
		'education' => $row->education,
		'created_datetime' => $row->created_datetime,
		'updated_datetime' => $row->updated_datetime,
		'created_by' => $row->created_by,
		'updated_by' => $row->updated_by,
	    );
            $this->load->view('partners/partners_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('partners'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('partners/create_action'),
	    'id' => set_value('id'),
	    'language_code' => set_value('language_code'),
	    'fullname' => set_value('fullname'),
	    'title' => set_value('title'),
	    'description_1' => set_value('description_1'),
	    'description_2' => set_value('description_2'),
	    'photo' => set_value('photo'),
	    'education' => set_value('education'),
	    'created_datetime' => set_value('created_datetime'),
	    'updated_datetime' => set_value('updated_datetime'),
	    'created_by' => set_value('created_by'),
	    'updated_by' => set_value('updated_by'),
	);
        $this->load->view('partners/partners_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'language_code' => $this->input->post('language_code',TRUE),
		'fullname' => $this->input->post('fullname',TRUE),
		'title' => $this->input->post('title',TRUE),
		'description_1' => $this->input->post('description_1',TRUE),
		'description_2' => $this->input->post('description_2',TRUE),
		'photo' => $this->input->post('photo',TRUE),
		'education' => $this->input->post('education',TRUE),
		'created_datetime' => $this->input->post('created_datetime',TRUE),
		'updated_datetime' => $this->input->post('updated_datetime',TRUE),
		'created_by' => $this->input->post('created_by',TRUE),
		'updated_by' => $this->input->post('updated_by',TRUE),
	    );

            $this->Partners_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('partners'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Partners_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('partners/update_action'),
		'id' => set_value('id', $row->id),
		'language_code' => set_value('language_code', $row->language_code),
		'fullname' => set_value('fullname', $row->fullname),
		'title' => set_value('title', $row->title),
		'description_1' => set_value('description_1', $row->description_1),
		'description_2' => set_value('description_2', $row->description_2),
		'photo' => set_value('photo', $row->photo),
		'education' => set_value('education', $row->education),
		'created_datetime' => set_value('created_datetime', $row->created_datetime),
		'updated_datetime' => set_value('updated_datetime', $row->updated_datetime),
		'created_by' => set_value('created_by', $row->created_by),
		'updated_by' => set_value('updated_by', $row->updated_by),
	    );
            $this->load->view('partners/partners_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('partners'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('id', TRUE));
        } else {
            $data = array(
		'language_code' => $this->input->post('language_code',TRUE),
		'fullname' => $this->input->post('fullname',TRUE),
		'title' => $this->input->post('title',TRUE),
		'description_1' => $this->input->post('description_1',TRUE),
		'description_2' => $this->input->post('description_2',TRUE),
		'photo' => $this->input->post('photo',TRUE),
		'education' => $this->input->post('education',TRUE),
		'created_datetime' => $this->input->post('created_datetime',TRUE),
		'updated_datetime' => $this->input->post('updated_datetime',TRUE),
		'created_by' => $this->input->post('created_by',TRUE),
		'updated_by' => $this->input->post('updated_by',TRUE),
	    );

            $this->Partners_model->update($this->input->post('id', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('partners'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Partners_model->get_by_id($id);

        if ($row) {
            $this->Partners_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('partners'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('partners'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('language_code', 'language code', 'trim|required');
	$this->form_validation->set_rules('fullname', 'fullname', 'trim|required');
	$this->form_validation->set_rules('title', 'title', 'trim|required');
	$this->form_validation->set_rules('description_1', 'description 1', 'trim|required');
	$this->form_validation->set_rules('description_2', 'description 2', 'trim|required');
	$this->form_validation->set_rules('photo', 'photo', 'trim|required');
	$this->form_validation->set_rules('education', 'education', 'trim|required');
	$this->form_validation->set_rules('created_datetime', 'created datetime', 'trim|required');
	$this->form_validation->set_rules('updated_datetime', 'updated datetime', 'trim|required');
	$this->form_validation->set_rules('created_by', 'created by', 'trim|required');
	$this->form_validation->set_rules('updated_by', 'updated by', 'trim|required');

	$this->form_validation->set_rules('id', 'id', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "partners.xls";
        $judul = "partners";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Language Code");
	xlsWriteLabel($tablehead, $kolomhead++, "Fullname");
	xlsWriteLabel($tablehead, $kolomhead++, "Title");
	xlsWriteLabel($tablehead, $kolomhead++, "Description 1");
	xlsWriteLabel($tablehead, $kolomhead++, "Description 2");
	xlsWriteLabel($tablehead, $kolomhead++, "Photo");
	xlsWriteLabel($tablehead, $kolomhead++, "Education");
	xlsWriteLabel($tablehead, $kolomhead++, "Created Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated Datetime");
	xlsWriteLabel($tablehead, $kolomhead++, "Created By");
	xlsWriteLabel($tablehead, $kolomhead++, "Updated By");

	foreach ($this->Partners_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->language_code);
	    xlsWriteLabel($tablebody, $kolombody++, $data->fullname);
	    xlsWriteLabel($tablebody, $kolombody++, $data->title);
	    xlsWriteLabel($tablebody, $kolombody++, $data->description_1);
	    xlsWriteLabel($tablebody, $kolombody++, $data->description_2);
	    xlsWriteLabel($tablebody, $kolombody++, $data->photo);
	    xlsWriteLabel($tablebody, $kolombody++, $data->education);
	    xlsWriteLabel($tablebody, $kolombody++, $data->created_datetime);
	    xlsWriteLabel($tablebody, $kolombody++, $data->updated_datetime);
	    xlsWriteNumber($tablebody, $kolombody++, $data->created_by);
	    xlsWriteNumber($tablebody, $kolombody++, $data->updated_by);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=partners.doc");

        $data = array(
            'partners_data' => $this->Partners_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('partners/partners_doc',$data);
    }

}

/* End of file Partners.php */
/* Location: ./application/controllers/Partners.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-05-09 06:26:50 */
/* http://harviacode.com */