<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');

class Cache extends CI_Controller
{
    function __construct()
    {
        parent::__construct();

        $this->load->model('Cache_model');
        $this->load->library('form_validation');

        if(!$this->session->userdata('logined') || $this->session->userdata('logined') != true)
        {
            redirect('/');
        }        
	$this->load->library('datatables');
    }

    public function index()
    {
        $this->load->view('cache/cache_list');
    } 
    
    public function json() {
        header('Content-Type: application/json');
        echo $this->Cache_model->json();
    }

    public function read($id) 
    {
        $row = $this->Cache_model->get_by_id($id);
        if ($row) {
            $data = array(
		'key' => $row->key,
		'value' => $row->value,
		'expiration' => $row->expiration,
	    );
            $this->load->view('cache/cache_read', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('cache'));
        }
    }

    public function create() 
    {
        $data = array(
            'button' => 'Create',
            'action' => site_url('cache/create_action'),
	    'key' => set_value('key'),
	    'value' => set_value('value'),
	    'expiration' => set_value('expiration'),
	);
        $this->load->view('cache/cache_form', $data);
    }
    
    public function create_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->create();
        } else {
            $data = array(
		'value' => $this->input->post('value',TRUE),
		'expiration' => $this->input->post('expiration',TRUE),
	    );

            $this->Cache_model->insert($data);
            $this->session->set_flashdata('message', 'Create Record Success');
            redirect(site_url('cache'));
        }
    }
    
    public function update($id) 
    {
        $row = $this->Cache_model->get_by_id($id);

        if ($row) {
            $data = array(
                'button' => 'Update',
                'action' => site_url('cache/update_action'),
		'key' => set_value('key', $row->key),
		'value' => set_value('value', $row->value),
		'expiration' => set_value('expiration', $row->expiration),
	    );
            $this->load->view('cache/cache_form', $data);
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('cache'));
        }
    }
    
    public function update_action() 
    {
        $this->_rules();

        if ($this->form_validation->run() == FALSE) {
            $this->update($this->input->post('key', TRUE));
        } else {
            $data = array(
		'value' => $this->input->post('value',TRUE),
		'expiration' => $this->input->post('expiration',TRUE),
	    );

            $this->Cache_model->update($this->input->post('key', TRUE), $data);
            $this->session->set_flashdata('message', 'Update Record Success');
            redirect(site_url('cache'));
        }
    }
    
    public function delete($id) 
    {
        $row = $this->Cache_model->get_by_id($id);

        if ($row) {
            $this->Cache_model->delete($id);
            $this->session->set_flashdata('message', 'Delete Record Success');
            redirect(site_url('cache'));
        } else {
            $this->session->set_flashdata('message', 'Record Not Found');
            redirect(site_url('cache'));
        }
    }

    public function _rules() 
    {
	$this->form_validation->set_rules('value', 'value', 'trim|required');
	$this->form_validation->set_rules('expiration', 'expiration', 'trim|required');

	$this->form_validation->set_rules('key', 'key', 'trim');
	$this->form_validation->set_error_delimiters('<span class="text-danger">', '</span>');
    }

    public function excel()
    {
        $this->load->helper('exportexcel');
        $namaFile = "cache.xls";
        $judul = "cache";
        $tablehead = 0;
        $tablebody = 1;
        $nourut = 1;
        //penulisan header
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0,pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");
        header("Content-Disposition: attachment;filename=" . $namaFile . "");
        header("Content-Transfer-Encoding: binary ");

        xlsBOF();

        $kolomhead = 0;
        xlsWriteLabel($tablehead, $kolomhead++, "No");
	xlsWriteLabel($tablehead, $kolomhead++, "Value");
	xlsWriteLabel($tablehead, $kolomhead++, "Expiration");

	foreach ($this->Cache_model->get_all() as $data) {
            $kolombody = 0;

            //ubah xlsWriteLabel menjadi xlsWriteNumber untuk kolom numeric
            xlsWriteNumber($tablebody, $kolombody++, $nourut);
	    xlsWriteLabel($tablebody, $kolombody++, $data->value);
	    xlsWriteNumber($tablebody, $kolombody++, $data->expiration);

	    $tablebody++;
            $nourut++;
        }

        xlsEOF();
        exit();
    }

    public function word()
    {
        header("Content-type: application/vnd.ms-word");
        header("Content-Disposition: attachment;Filename=cache.doc");

        $data = array(
            'cache_data' => $this->Cache_model->get_all(),
            'start' => 0
        );
        
        $this->load->view('cache/cache_doc',$data);
    }

}

/* End of file Cache.php */
/* Location: ./application/controllers/Cache.php */
/* Please DO NOT modify this information : */
/* Generated by Harviacode Codeigniter CRUD Generator 2020-05-09 06:26:49 */
/* http://harviacode.com */