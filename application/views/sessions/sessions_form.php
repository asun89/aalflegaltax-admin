<?php $this->load->view('templates/header');?>
<div class="row" style="margin-bottom: 20px">
            <div class="col-md-4">
                <h2>Sessions <?php echo $button ?></h2>
            </div>
            <div class="col-md-8 text-center">
                <div id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
        </div>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="int">User Id <?php echo form_error('user_id') ?></label>
            <input type="text" class="form-control" name="user_id" id="user_id" placeholder="User Id" value="<?php echo $user_id; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Ip Address <?php echo form_error('ip_address') ?></label>
            <input type="text" class="form-control" name="ip_address" id="ip_address" placeholder="Ip Address" value="<?php echo $ip_address; ?>" />
        </div>
	    <div class="form-group">
            <label for="user_agent">User Agent <?php echo form_error('user_agent') ?></label>
            <textarea class="form-control" rows="3" name="user_agent" id="user_agent" placeholder="User Agent"><?php echo $user_agent; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="payload">Payload <?php echo form_error('payload') ?></label>
            <textarea class="form-control" rows="3" name="payload" id="payload" placeholder="Payload"><?php echo $payload; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="int">Last Activity <?php echo form_error('last_activity') ?></label>
            <input type="text" class="form-control" name="last_activity" id="last_activity" placeholder="Last Activity" value="<?php echo $last_activity; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('sessions') ?>" class="btn btn-default">Cancel</a>
	</form><?php $this->load->view('templates/footer');?>