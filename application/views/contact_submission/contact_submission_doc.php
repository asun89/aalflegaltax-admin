<!doctype html>
<html>
    <head>
        <title>SOCIANOVATION - Web Administration</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Contact_submission List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Name</th>
		<th>Email</th>
		<th>Phone</th>
		<th>Subject</th>
		<th>Message</th>
		<th>Deleted At</th>
		<th>Created At</th>
		<th>Updated At</th>
		
            </tr><?php
            foreach ($contact_submission_data as $contact_submission)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $contact_submission->name ?></td>
		      <td><?php echo $contact_submission->email ?></td>
		      <td><?php echo $contact_submission->phone ?></td>
		      <td><?php echo $contact_submission->subject ?></td>
		      <td><?php echo $contact_submission->message ?></td>
		      <td><?php echo $contact_submission->deleted_at ?></td>
		      <td><?php echo $contact_submission->created_at ?></td>
		      <td><?php echo $contact_submission->updated_at ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>