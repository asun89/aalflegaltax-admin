<!doctype html>
<html>
    <head>
        <title>SOCIANOVATION - Web Administration</title>
        <link rel="stylesheet" href="<?php echo base_url('assets/bootstrap/css/bootstrap.min.css') ?>"/>
        <style>
            .word-table {
                border:1px solid black !important; 
                border-collapse: collapse !important;
                width: 100%;
            }
            .word-table tr th, .word-table tr td{
                border:1px solid black !important; 
                padding: 5px 10px;
            }
        </style>
    </head>
    <body>
        <h2>Partners List</h2>
        <table class="word-table" style="margin-bottom: 10px">
            <tr>
                <th>No</th>
		<th>Language Code</th>
		<th>Fullname</th>
		<th>Title</th>
		<th>Description 1</th>
		<th>Description 2</th>
		<th>Photo</th>
		<th>Education</th>
		<th>Created Datetime</th>
		<th>Updated Datetime</th>
		<th>Created By</th>
		<th>Updated By</th>
		
            </tr><?php
            foreach ($partners_data as $partners)
            {
                ?>
                <tr>
		      <td><?php echo ++$start ?></td>
		      <td><?php echo $partners->language_code ?></td>
		      <td><?php echo $partners->fullname ?></td>
		      <td><?php echo $partners->title ?></td>
		      <td><?php echo $partners->description_1 ?></td>
		      <td><?php echo $partners->description_2 ?></td>
		      <td><?php echo $partners->photo ?></td>
		      <td><?php echo $partners->education ?></td>
		      <td><?php echo $partners->created_datetime ?></td>
		      <td><?php echo $partners->updated_datetime ?></td>
		      <td><?php echo $partners->created_by ?></td>
		      <td><?php echo $partners->updated_by ?></td>	
                </tr>
                <?php
            }
            ?>
        </table>
    </body>
</html>