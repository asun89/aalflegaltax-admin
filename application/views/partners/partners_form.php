<?php $this->load->view('templates/header');?>
<div class="row" style="margin-bottom: 20px">
            <div class="col-md-4">
                <h2>Partners <?php echo $button ?></h2>
            </div>
            <div class="col-md-8 text-center">
                <div id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
        </div>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Language Code <?php echo form_error('language_code') ?></label>
            <input type="text" class="form-control" name="language_code" id="language_code" placeholder="Language Code" value="<?php echo $language_code; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Fullname <?php echo form_error('fullname') ?></label>
            <input type="text" class="form-control" name="fullname" id="fullname" placeholder="Fullname" value="<?php echo $fullname; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Title <?php echo form_error('title') ?></label>
            <input type="text" class="form-control" name="title" id="title" placeholder="Title" value="<?php echo $title; ?>" />
        </div>
	    <div class="form-group">
            <label for="description_1">Description 1 <?php echo form_error('description_1') ?></label>
            <textarea class="form-control" rows="3" name="description_1" id="description_1" placeholder="Description 1"><?php echo $description_1; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="description_2">Description 2 <?php echo form_error('description_2') ?></label>
            <textarea class="form-control" rows="3" name="description_2" id="description_2" placeholder="Description 2"><?php echo $description_2; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="varchar">Photo <?php echo form_error('photo') ?></label>
            <input type="text" class="form-control" name="photo" id="photo" placeholder="Photo" value="<?php echo $photo; ?>" />
        </div>
	    <div class="form-group">
            <label for="education">Education <?php echo form_error('education') ?></label>
            <textarea class="form-control" rows="3" name="education" id="education" placeholder="Education"><?php echo $education; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="datetime">Created Datetime <?php echo form_error('created_datetime') ?></label>
            <input type="text" class="form-control" name="created_datetime" id="created_datetime" placeholder="Created Datetime" value="<?php echo $created_datetime; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Updated Datetime <?php echo form_error('updated_datetime') ?></label>
            <input type="text" class="form-control" name="updated_datetime" id="updated_datetime" placeholder="Updated Datetime" value="<?php echo $updated_datetime; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Created By <?php echo form_error('created_by') ?></label>
            <input type="text" class="form-control" name="created_by" id="created_by" placeholder="Created By" value="<?php echo $created_by; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Updated By <?php echo form_error('updated_by') ?></label>
            <input type="text" class="form-control" name="updated_by" id="updated_by" placeholder="Updated By" value="<?php echo $updated_by; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('partners') ?>" class="btn btn-default">Cancel</a>
	</form><?php $this->load->view('templates/footer');?>