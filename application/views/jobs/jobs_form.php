<?php $this->load->view('templates/header');?>
<div class="row" style="margin-bottom: 20px">
            <div class="col-md-4">
                <h2>Jobs <?php echo $button ?></h2>
            </div>
            <div class="col-md-8 text-center">
                <div id="message">
                    <?php echo $this->session->userdata('message') <> '' ? $this->session->userdata('message') : ''; ?>
                </div>
            </div>
        </div>
        <form action="<?php echo $action; ?>" method="post">
	    <div class="form-group">
            <label for="varchar">Language Code <?php echo form_error('language_code') ?></label>
            <input type="text" class="form-control" name="language_code" id="language_code" placeholder="Language Code" value="<?php echo $language_code; ?>" />
        </div>
	    <div class="form-group">
            <label for="varchar">Job Title <?php echo form_error('job_title') ?></label>
            <input type="text" class="form-control" name="job_title" id="job_title" placeholder="Job Title" value="<?php echo $job_title; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Slot <?php echo form_error('slot') ?></label>
            <input type="text" class="form-control" name="slot" id="slot" placeholder="Slot" value="<?php echo $slot; ?>" />
        </div>
	    <div class="form-group">
            <label for="requirements">Requirements <?php echo form_error('requirements') ?></label>
            <textarea class="form-control" rows="3" name="requirements" id="requirements" placeholder="Requirements"><?php echo $requirements; ?></textarea>
        </div>
	    <div class="form-group">
            <label for="datetime">Created Datetime <?php echo form_error('created_datetime') ?></label>
            <input type="text" class="form-control" name="created_datetime" id="created_datetime" placeholder="Created Datetime" value="<?php echo $created_datetime; ?>" />
        </div>
	    <div class="form-group">
            <label for="datetime">Updated Datetime <?php echo form_error('updated_datetime') ?></label>
            <input type="text" class="form-control" name="updated_datetime" id="updated_datetime" placeholder="Updated Datetime" value="<?php echo $updated_datetime; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Created By <?php echo form_error('created_by') ?></label>
            <input type="text" class="form-control" name="created_by" id="created_by" placeholder="Created By" value="<?php echo $created_by; ?>" />
        </div>
	    <div class="form-group">
            <label for="int">Updated By <?php echo form_error('updated_by') ?></label>
            <input type="text" class="form-control" name="updated_by" id="updated_by" placeholder="Updated By" value="<?php echo $updated_by; ?>" />
        </div>
	    <input type="hidden" name="id" value="<?php echo $id; ?>" /> 
	    <button type="submit" class="btn btn-primary"><?php echo $button ?></button> 
	    <a href="<?php echo site_url('jobs') ?>" class="btn btn-default">Cancel</a>
	</form><?php $this->load->view('templates/footer');?>